<?php
/**
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Campaigns Main
 */

get_header(); ?>
<section class="single-col page-content primary" role="main">
		
	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>


	        <article class="container_full content_band">
	        	<div class="container_boxed--narrow">
	        		<?php the_field('1col_content_area');?>
	        	</div>
	        </article>
			
			<div class="container_full">
			<?php

			// check if the repeater field has rows of data
			if( have_rows('campaign_item') ):?>
			

			    <?php while ( have_rows('campaign_item') ) : the_row();?>
				
				<div class="campaign-item">
					<div class = "campaign-item__image">
					   <?php
							the_sub_field('campaign_media');
						?>
					</div>
						        	
					<div class="campaign-item__content container_boxed--narrow content_band--small">
						<h3 class="center">
							<?php
							the_sub_field('campaign_title');
							?>
						</h3>
						<?php
							the_sub_field('campaign_content');
						?>
						
						<div class="btn-container center">
							<?php if( get_sub_field('campaign_link') ): ?>
							<a class="btn-primary-outline center" href="<?php the_sub_field('campaign_link');?>" title="Read more about Campaign">
								<?php _e('Find out more','mooncupmain'); ?>
							</a>
						<?php endif; ?>
						</div>
						
					</div>

				</div>
						       
			    <?php endwhile;?>
			</div>
			<?php 

			else :

			    // no rows found

			endif;

			?>

			<aside class="page-outro container_boxed content_band--lined">
	        	<div class="container_boxed--narrow content_band--small">
	        		<?php the_field('footer_area');?>
	        	</div>
	        </aside>
	
</section>

<?php get_footer(); ?>
