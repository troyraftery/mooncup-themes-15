<div id="affwp-affiliate-dashboard-creatives" class="affwp-tab-content">

	<h4><?php _e( 'Mooncup Links', 'affiliatewp' ); ?></h4>
	<p>
		Occasionally we post links to promotions and campaigns that may be of interest to your visitors here.
		You can copy and paste these links into your blog, forum or website. These links will be unique to you and
		you will receive click or sales credits 'to your account' for visitors coming to our site through these links in the usual way.
		We will email to let you know when we add new links.</p>
	<?php
	$per_page  = 30;
	$page      = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$pages     = absint( ceil( affiliate_wp()->creatives->count( array( 'status' => 'active' ) ) / $per_page ) );
	$args      = array(
		'number' => $per_page,
		'offset' => $per_page * ( $page - 1 )
	);
	$creatives = affiliate_wp()->creative->affiliate_creatives( $args );
	?>

	<?php if ( $creatives ) : ?>

		<?php do_action( 'affwp_before_creatives' ); ?>

		<?php echo affiliate_wp()->creative->affiliate_creatives( $args ); ?>

		<?php if ( $pages > 1 ) : ?>

			<p class="affwp-pagination">
				<?php
				echo paginate_links(
					array(
						'current'  => $page,
						'total'    => $pages,
						'add_args' => array(
							'tab' => 'creatives',
						),
					)
				);
				?>
			</p>

		<?php endif; ?>

		<?php do_action( 'affwp_after_creatives' ); ?>

	<?php else : ?>

		<p class="affwp-no-results"><?php _e( 'Sorry, there are currently no links available.', 'affiliatewp' ); ?></p>

	<?php endif; ?>
<p></p>
</div>
