<?php
/**
 * Mooncup Main template for displaying Single-Posts
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */

get_header(); ?>

<section class="using-your-mooncup using-mooncup-item page-content primary" role="main">

    <article class="container_full splash-content-block">
    	<?php if (has_post_thumbnail( $post->ID ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
    	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php echo $image[0]; ?>');">
        
        	<div class="splash-content-overlay splash-header text-reverse">
        		<div class="container_full">
	        	<?php the_field('splash_content'); ?>
	        	</div>
        	</div>
        </div>
    	<?php endif; ?>
    </article>

    <section class="container_boxed content_band">
    	<aside class="sidebar col__4">
    		<ul class="sidebar"><?php
				if ( function_exists( 'dynamic_sidebar' ) ) :
					dynamic_sidebar( 'faq-sidebar' );
				endif; ?>
			</ul>
    	</aside>

    	<div class="faq-content col__8">

		    <div class="faq-item-post faq-content-item">
			    <?php
					if ( have_posts() ) : the_post();

						get_template_part( 'loop', 'usage' ); ?>

						<?php

					else :

						get_template_part( 'loop', 'empty' );

					endif;
				?>
			</div>

			<div class="link-container caps-text center">
				<a href="<?php print $_SERVER['HTTP_REFERER'];?>">BACK</a>
			</div>

			<div class="faq-form">

				<?php
				/*
				*  View value data (for debugging) turn on the FAQs enquiry form using checkbox on article page
				*/

				/*var_dump( get_field('form_on_off') );*/

				/*
				*  Conditional Statement
				*/

				if( get_field('form_on_off') )
				{
					echo do_shortcode('[contact-form-7 id="1629" title="FAQ Form"]');
				}
				else
				{
				    echo "";
				}

				/*
				*  Query posts for a true/false value.
				*  This method uses the meta_query param to match the string "1" to the database value "1|0"
				*/

				$posts = get_posts(array(
					'meta_query' => array(
						array(
							'key' => 'form_on_off',
							'value' => '1',
							'compare' => '=='
						)
					)
				));

				if( $posts )
				{
					foreach( $posts as $post )
					{
						setup_postdata( $post );

						// ...

					}

					wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
				}

				?>

			</div>

			<div class="related-testimonials">

				<?php
				/*
				*  View value data (for debugging)
				*/

				/*var_dump( get_field('testimonial_control') );*/

				/*
				*  Conditional Statement
				*/

				if( get_field('testimonial_control') )
				{?>
				    <h2>Hear from our mooncup users...</h2>
					<div class="container_full">
						<?php
			            $args = array(
			                'post_type' => 'testimonial',
			                'posts_per_page' => 3,
			                'testimonials' => 'featured',
			            );
			            query_posts($args);
			            if ( have_posts() ):
			                while ( have_posts() ) :
			                    the_post();
			                    get_template_part( 'loop', get_post_type() );
			                endwhile;
			            else :
			                get_template_part( 'loop', 'testimonial' );
			            endif;
			            wp_reset_query();
			            ?>
					</div>
					<div class="link-container caps-text center">
						<a href="/testimonial" title="All Questions">READ MORE</a>
					</div>	
					<?php
				}
				else
				{
				    echo "";
				}

				/*
				*  Query posts for a true/false value.
				*  This method uses the meta_query param to match the string "1" to the database value "1|0"
				*/

				$posts = get_posts(array(
					'meta_query' => array(
						array(
							'key' => 'testimonial_control',
							'value' => '1',
							'compare' => '=='
						)
					)
				));

				if( $posts )
				{
					foreach( $posts as $post )
					{
						setup_postdata( $post );

						// ...

					}

					wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
				}

				?>
			
			</div>

			<div class="related-blog-posts">

				<?php
				/*
				*  View value data (for debugging)
				*/

				/*var_dump( get_field('blog_control') );*/

				/*
				*  Conditional Statement
				*/

				if( get_field('blog_control') )
				{?>
				    <h2>Related Articles from the Mooncup Blog</h2>
					<div class="container_full blog-LP three--col-grid">
						<?php

							$tags = wp_get_post_tags($post->ID);

							if ($tags) {

								$tag_ids = array();
								//$events_query = new WP_Query( 'category_name=Event+News&posts_per_page=8');
								foreach($tags as $individual_tag):
									$tag_ids[] = $individual_tag->term_id;
								endforeach;

								$args = array(
									'post_type' => array('blog','usage'),
									'tag__in' => $tag_ids,
									'posts_per_page' => 12
								);
								query_posts($args);
								if (have_posts()):
									while (have_posts()) : the_post();
										get_template_part('loop','blog');
									endwhile;
								else :
									get_template_part('loop', 'empty');
								endif;
								wp_reset_query();
							}
						?>
		        	</div>
					<?php
				}
				else
				{
				    echo "";
				}

				/*
				*  Query posts for a true/false value.
				*  This method uses the meta_query param to match the string "1" to the database value "1|0"
				*/

				$posts = get_posts(array(
					'meta_query' => array(
						array(
							'key' => 'blog_control',
							'value' => '1',
							'compare' => '=='
						)
					)
				));

				if( $posts )
				{
					foreach( $posts as $post )
					{
						setup_postdata( $post );

						// ...

					}

					wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
				}

				?>

			</div>

    	</div>

    </section>

</section>

<?php get_footer(); ?>
